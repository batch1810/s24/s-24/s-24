//Mini-Activity
//Insert back Stephen Hawking and Jane Doe to our users collection
/*
firstName: Jane
lastName: Doe
age: 21
contact: (phone, email)
courses: CSS, JavaScript, Python
department: Operations

firstName: Stephen
lastName: Hawking
age: 76
contact: (phone, email)
courses: Python, React, PHP
department: HR
*/

//[Section] Comparison Query Operators
//$gt/$gte operator
/*
    allows us to find documents that have field number values greater than or equal to a specified value

    Syntax:
        db.collectionName.find({"field": {$gt: "value"}})
        db.collectionName.find({"field": {$gte: "value"}})
*/

db.users.find({"age": {$gt: 50}});
db.users.find({"age": {$gte: 50}});

//$lt/$lte operator
/*
    allows us to find documents that have field number values less than or equal to a specified value.

    Syntax:
        db.collectionName.find({"field": {$lt: "value"}})
        db.collectionName.find({"field": {$lte: "value"}})
*/
db.users.find({ "age" : { $lt : 50 } });
db.users.find({ "age" : { $lte : 50 } });


//$ne operator
/*
    allows us to find documents that have field number values not equal to a specified value.

    Syntax:
        db.collectionName.find({"field": {$ne: "value"}})
*/
db.users.find({"age": {$ne: 82}});

//$in operator
/*
    allows us to find documents with specific match criteria one field using different values.

    Syntax:
        db.collectionName.find({"field": {$in: "value"}})
*/
db.users.find({"lastName": {$in: ["Hawking", "Doe"]}});
db.users.find({"courses": { $in: [ "HTML", "React" ]}});

//[Section] Logical Query Operators
//$or operator
/*
    allows us to find documents that match a single criteria from multiple provided search criteria.

    Syntax:
        db.collectionName.find({$or: [{"fieldA": "valueA"}, {"fieldB": "valueB"}]})
*/
db.users.find({$or: [{"firstName": "Neil"}, {"age": 25}]});
db.users.find({$or: [{"firstName": "Neil" }, { "age": {$gt: 30}}] });

//$and operator
/*
    allows us to find documents matching multiple criteria in a single field.

    Syntax:
        db.collectionName.find({$and: [{"fieldA": "valueA"}, {"fieldB": "valueB"}]});
*/
db.users.find({$and: [{"age": {$ne: 82}}, {"age": {$ne: 76}}]});

//[Section] Field Projection
//Inclusion
/*
    Allows us to include/add specific fields only when retrieving documents.
    The value provided is 1 to denote that the field is being included.

    Syntax:
        db.users.find({"criteria"}, {"field": 1});
*/
db.users.find(
    {"firstName": "Jane"},
    {
        "firstName": 1,
        "lastName": 1,
        "contact": 1
    }
);

//Exclusion
/*
    allows us to exclude/remove specific fields only when retrieving documents.
    The value provided is 0 to denote that the field is being excluded

    Syntax:
        db.users.find({"criteria"}, {"field": 0})
*/
db.users.find( 
    { 
        firstName: "Jane" 
    }, 
    { 
        contact: 0, 
        department: 0 
    } 
);

//Suppressing the ID field
/*
    allows us to exclude the "_id" field when retrieving documents
    When using field projection, field inclusion and exclusion may not be used at the same time
    _id is the only exception to this rule

    Syntax:
        db.users.find({"criteria"}, {"_id": 0})
*/
db.users.find(
    {"firstName": "Jane"},
    {
        "firstName": 1,
        "lastName": 1,
        "contact": 1,
        "_id": 0
    }
);

//Returning Specific Fields in Embedded Documents
db.users.find(
    {"firstName": "Jane"},
    {
        "firstName": 1,
        "lastName": 1,
        "contact.phone": 1
    }
);

//Suppressing Specific Fields in Embedded Documents
db.users.find(
    {"firstName": "Jane"},
    {
        "contact.phone": 0
    }
);

//[Section] Evaluation Query Operators
//$regex operator
/*
    $regex -> Regular Expression
    Allows us to find documents that match a specific string pattern using regular expressions.

    Syntax:
        db.users.find({"field": $regex: "pattern", $options: "$optionValue"});
*/
//Case sensitive query
db.users.find({"firstName": {$regex: "N"}});

//Case insensitive query
db.users.find({"firstName": {$regex: "n", $options: "$i"}})
